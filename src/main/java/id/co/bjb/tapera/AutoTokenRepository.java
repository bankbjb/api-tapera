/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bjb.tapera;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 *
 * @author LENOVO
 */
@Component
public class AutoTokenRepository implements TokenRepository {

    @Autowired
    private CacheManager cacheManager;
    
    @Override
    @Cacheable("token")
    public Token getToken() {
        if (cacheManager.getCache("token") == null) {
            return new Token("", "", 1);
        }
        
        return null;
    }
    
}
