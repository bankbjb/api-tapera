/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bjb.tapera;

import org.springframework.cache.annotation.Cacheable;

/**
 *
 * @author LENOVO
 */
public class Token {
    
    private String type;
    private String token;
    private int expiredInSeconds;

    public Token(String type, String token, int expiredInSeconds) {
        this.type = type;
        this.token = token;
        this.expiredInSeconds = expiredInSeconds;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getExpiredInSeconds() {
        return expiredInSeconds;
    }

    public void setExpiredInSeconds(int expiredInSeconds) {
        this.expiredInSeconds = expiredInSeconds;
    }

}
