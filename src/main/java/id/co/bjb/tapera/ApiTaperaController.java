/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bjb.tapera;

import id.co.bjb.tapera.service.TaperaService;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author LENOVO
 */
@RestController
@RequestMapping("/api-tapera")
public class ApiTaperaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiTaperaController.class);

    @Autowired
    private ConfigProperties properties;
    @Autowired
    private TaperaService taperaService;

    @PostMapping(path = "/tapera", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> apiTapera(@RequestBody String requestBody) {
        JSONObject request = new JSONObject(requestBody);

        JSONObject response = new JSONObject(requestBody);
        JSONObject paramResponse;

        String function = request.getString("FUNC");
        LOGGER.info("###INFO: Request for function " + function);

        switch (function) {
            case "PARTICIPANT-CHECK":
                paramResponse = taperaService.postInquiryPeserta(request.getJSONObject("PARAM"));
                response.put("RESP_DATA", paramResponse);

                break;
            case "NOTIF-SLIK":
                paramResponse = taperaService.postNotifSlik(request.getJSONObject("PARAM"));
                response.put("RESP_DATA", paramResponse);

                break;
            case "VERITAP-1":
                paramResponse = taperaService.postVerifikasiPertama(request.getJSONObject("PARAM"));
                response.put("RESP_DATA", paramResponse);

                break;
            case "NOTIF-SPK":
                paramResponse = taperaService.postNotifSpk(request.getJSONObject("PARAM"));
                response.put("RESP_DATA", paramResponse);

                break;
            case "VERITAP-2":
                paramResponse = taperaService.postVerifikasiKedua(request.getJSONObject("PARAM"));
                response.put("RESP_DATA", paramResponse);

                break;
            case "DISBURSE-REQUEST":
                paramResponse = taperaService.postPengajuanPencairan(request.getJSONObject("PARAM"));
                response.put("RESP_DATA", paramResponse);

                break;
            case "DISBURSE-APPROVAL-STATUS":
                paramResponse = taperaService.postCekStatusPengajuanPencairan(request.getJSONObject("PARAM"));
                response.put("RESP_DATA", paramResponse);

                break;
            case "SCREENING":
                paramResponse = taperaService.postNotifSlik(buildRequestNotifSlik(request.getJSONObject("PARAM")));

                if (paramResponse.getInt("responseCode") != 2000) {
                    response.put("RESP", "INVALID_SLIK_REQUEST");
                    response.put("RESP_CODE", paramResponse.getInt("responseCode"));
                    response.put("RESP_DATA", paramResponse);

                    return ResponseEntity.status(HttpStatus.OK).body(response.toString());
                }

                JSONArray arrResponsePeserta = new JSONArray();
                JSONArray arrRespSlik = paramResponse.getJSONArray("peserta");

                for (int i = 0; i < arrRespSlik.length(); i++) {
                    if (arrRespSlik.getJSONObject(i).getInt("code") != 2000) {
                        arrResponsePeserta.put(arrRespSlik.getJSONObject(i));
                    }
                }

                JSONObject veritap1Response = taperaService.postVerifikasiPertama(buildRequestVerifikasiPertama(request.getJSONObject("PARAM"), arrRespSlik));

                if (veritap1Response.getInt("responseCode") != 2000) {
                    response.put("RESP", "INVALID_VERITAP1_REQUEST");
                    response.put("RESP_CODE", veritap1Response.getInt("responseCode"));
                    response.put("RESP_DATA", veritap1Response);

                    return ResponseEntity.status(HttpStatus.OK).body(response.toString());
                }

                JSONArray arrRespVeritap1 = veritap1Response.getJSONArray("peserta");

                for (int i = 0; i < arrRespVeritap1.length(); i++) {
                    arrResponsePeserta.put(arrRespVeritap1.getJSONObject(i));
                }

                paramResponse.put("peserta", arrResponsePeserta);

                response.put("RESP_DATA", paramResponse);

                break;
            default:
                LOGGER.error("###ERROR: Undefined for function " + function);

                response.put("RESP", "UNDEFINED_FUNCTION");
                response.put("RESP_CODE", HttpStatus.BAD_REQUEST.value());
                response.put("RESP_DATA", "UNDEFINED_FUNCTION");

                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response.toString());
        }

        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
    }

    @PostMapping(path = "/token"
    //            , consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    public void postToken() {
//    public String postToken(@RequestBody String requestBody) {
//        String url = "https://apidev.tapera.go.id:8443/security/oauth2/token";  //DEV
        String url = properties.getHost() + "/security/oauth2/token"; //PRD

//        JSONObject jsonRequest = new JSONObject("{\"client_id\":\"t9kao8cyWBofVM4hYfvinyQe96jBoPL7\",\"grant_type\":\"client_credentials\",\"client_secret\":\"Xz9ABobe8oAHSmNZ13WCjCeglRJ0MwAA\"}");   //DEV
//        JSONObject jsonRequest = new JSONObject("{\"client_id\":\"eKVRkRFomvmjmtGF6Cimg1qMwn0uC0bQ\",\"grant_type\":\"client_credentials\",\"client_secret\":\"fToySlY90gMMqxi1y9wSjzO76fiLoJKq\"}");   //PRD
        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("grant_type", "client_credentials");
        jsonRequest.put("client_id", properties.getClientId());
        jsonRequest.put("client_secret", properties.getClientSecret());

//        String PROXY_SERVER_HOST = "10.12.14.8";
//        int PROXY_SERVER_PORT = 3128;
        String PROXY_SERVER_HOST = properties.getProxyHost();
        int PROXY_SERVER_PORT = Integer.valueOf(properties.getProxyPort());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_SERVER_HOST, PROXY_SERVER_PORT));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(proxy);

        ignoreCertificates();
//        trustSelfSignedSSL();
        RestTemplate restTemplate = new RestTemplate(requestFactory);

        try {
            LOGGER.info("###REQ: " + jsonRequest);

            HttpEntity<String> requestStr = new HttpEntity<>(jsonRequest.toString(), headers);

            String result = restTemplate.postForObject(url, requestStr, String.class);

            LOGGER.info("###RES: " + result);
        } catch (RestClientException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        } catch (Exception ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
    }

    private void ignoreCertificates() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
//            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (KeyManagementException | NoSuchAlgorithmException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
    }

    private void ignoreCertificates2() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
//            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (KeyManagementException | NoSuchAlgorithmException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
    }

    public static void trustSelfSignedSSL() {
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            X509TrustManager tm = new X509TrustManager() {

                @Override
                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            ctx.init(null, new TrustManager[]{tm}, null);
            SSLContext.setDefault(ctx);
        } catch (KeyManagementException | NoSuchAlgorithmException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
    }

//    private JSONObject buildRequestNotifSlik(JSONObject requestParam) {
//        JSONObject slikRequest = new JSONObject(requestParam.toString());
//        JSONArray pesertaArray = new JSONArray();
//
//        ArrayList<String> keys1 = new ArrayList<>(Arrays.asList("cabang", "jumlah", "kodeMitra", "namaOperator", "peserta"));
//        ArrayList<String> keys2 = new ArrayList<>(Arrays.asList("hasilKPR", "kolektibilitas", "namaPeserta", "nomorPeserta"));
//
//        LOGGER.info("###INFO: Convert to NOTIF-SLIK request: " + requestParam);
//
//        Iterator<String> iterator1 = keys1.iterator();
//        while (iterator1.hasNext()) {
//            String key1 = iterator1.next();
//
//            if (key1.equals("peserta")) {
//                JSONArray requestArray = requestParam.getJSONArray(key1);
//
//                for (int i = 0; i < requestArray.length(); i++) {
//                    JSONObject peserta = new JSONObject();
//
//                    Iterator<String> iterator2 = keys2.iterator();
//                    while (iterator2.hasNext()) {
//                        String key2 = iterator2.next();
//
//                        peserta.put(key2, requestArray.getJSONObject(i).optJSONObject(key2));
//                    }
//
//                    pesertaArray.put(peserta);
//                }
//
//                slikRequest.put(key1, pesertaArray);
//            } else {
//                slikRequest.put(key1, requestParam.optJSONObject(key1));
//            }
//        }
//
//        return slikRequest;
//    }
    private JSONObject buildRequestNotifSlik(JSONObject requestParam) {
        JSONObject slikRequest = new JSONObject(requestParam.toString());
        JSONArray pesertaArray = new JSONArray();

        ArrayList<String> keys2 = new ArrayList<>(Arrays.asList("hasilKPR", "kolektibilitas", "namaPeserta", "nomorPeserta"));

        LOGGER.info("###INFO: Convert to NOTIF-SLIK request: " + requestParam);

        JSONArray requestArray = requestParam.getJSONArray("peserta");

        for (int i = 0; i < requestArray.length(); i++) {
            JSONObject peserta = new JSONObject();

            Iterator<String> iterator2 = keys2.iterator();
            while (iterator2.hasNext()) {
                String key2 = iterator2.next();

                peserta.put(key2, requestArray.getJSONObject(i).get(key2));
            }

            pesertaArray.put(peserta);
        }

        slikRequest.put("peserta", pesertaArray);

        return slikRequest;
    }

//    private JSONObject buildRequestVerifikasiPertama(JSONObject requestParam, JSONArray pesertaSlik) {
//        JSONObject veritap1Request = new JSONObject();
//        JSONArray pesertaArray = new JSONArray();
//
//        ArrayList<String> keys1 = new ArrayList<>(Arrays.asList("cabang", "jumlah", "kodeMitra", "namaOperator", "peserta"));
//        ArrayList<String> keys2 = new ArrayList<>(Arrays.asList("hasilKPR", "kolektibilitas", "namaPeserta", "nomorPeserta", "bentukPembiayaan", "besaranPenghasilan", "jenisPembiayaan", "lolosSlik", "namaPasangan", "nomorPasangan"));
//
//        LOGGER.info("###INFO: Convert to VERITAP-1 request: " + requestParam);
//
//        Iterator<String> iterator1 = keys1.iterator();
//        while (iterator1.hasNext()) {
//            String key1 = iterator1.next();
//
//            if (key1.equals("peserta")) {
//                JSONArray requestArray = requestParam.getJSONArray(key1);
//
//                for (int j = 0; j < pesertaSlik.length(); j++) {
//                    for (int i = 0; i < requestArray.length(); i++) {
//                        JSONObject slik = pesertaSlik.getJSONObject(j);
//                        JSONObject veritap1 = requestArray.getJSONObject(i);
//
//                        if (slik.getInt("code") == 2000
//                                && slik.getString("nomorPeserta").equals(veritap1.getString("nomorPeserta"))) {
//
//                            JSONObject peserta = new JSONObject();
//
//                            Iterator<String> iterator2 = keys2.iterator();
//                            while (iterator2.hasNext()) {
//                                String key2 = iterator2.next();
//
//                                if (key2.equals("lolosSlik")) {
//                                    peserta.put(key2, true);
//                                } else {
//                                    peserta.put(key2, requestArray.getJSONObject(i).get(key2));
//                                }
//                            }
//
//                            pesertaArray.put(peserta);
//                        }
//                    }
//                }
//
//                veritap1Request.put(key1, pesertaArray);
//            } else {
//                veritap1Request.put(key1, requestParam.optJSONObject(key1));
//            }
//        }
//
//        return veritap1Request;
//    }
    private JSONObject buildRequestVerifikasiPertama(JSONObject requestParam, JSONArray pesertaSlik) {
        JSONObject veritap1Request = new JSONObject(requestParam.toString());
        JSONArray pesertaArray = new JSONArray();

        ArrayList<String> keys2 = new ArrayList<>(Arrays.asList("namaPeserta", "nomorPeserta", "bentukPembiayaan", "besaranPenghasilan", "jenisPembiayaan", "lolosSlik", "namaPasangan", "nomorPasangan"));

        LOGGER.info("###INFO: Convert to VERITAP-1 request: " + requestParam);

        JSONArray requestArray = requestParam.getJSONArray("peserta");

        for (int j = 0; j < pesertaSlik.length(); j++) {
            for (int i = 0; i < requestArray.length(); i++) {
                JSONObject slik = pesertaSlik.getJSONObject(j);
                JSONObject veritap1 = requestArray.getJSONObject(i);

                if (slik.getInt("code") == 2000
                        && slik.getString("nomorPeserta").equals(veritap1.getString("nomorPeserta"))) {

                    JSONObject peserta = new JSONObject();

                    Iterator<String> iterator2 = keys2.iterator();
                    while (iterator2.hasNext()) {
                        String key2 = iterator2.next();

                        if (key2.equals("lolosSlik")) {
                            peserta.put(key2, true);
                        } else {
                            peserta.put(key2, requestArray.getJSONObject(i).get(key2));
                        }
                    }

                    pesertaArray.put(peserta);
                }
            }
        }

        veritap1Request.put("jumlah", pesertaArray.length());
        veritap1Request.put("peserta", pesertaArray);

        return veritap1Request;
    }
}
