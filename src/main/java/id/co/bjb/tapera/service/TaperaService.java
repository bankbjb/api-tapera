/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bjb.tapera.service;

import id.co.bjb.tapera.ConfigProperties;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author LENOVO
 */
@Service
public class TaperaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaperaService.class);
    
    @Autowired
    private ConfigProperties properties;
    
    private static final String PROXY_SERVER_HOST = "10.12.14.8";
    private static final int PROXY_SERVER_PORT = 3128;
    
    public JSONObject postCekStatusPengajuanPencairan(JSONObject param) {
//        String url = "https://apidev.tapera.go.id:8443/mitrapenyalur/v1/dev/pencairan/persetujuan"; //DEV
        String url = properties.getHost() + "/mitrapenyalur/v1/pencairan/persetujuan"; //PRD
        
        String token = postToken();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);
        
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_SERVER_HOST, PROXY_SERVER_PORT));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(proxy);
        
        ignoreCertificates();
        
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        try {
            LOGGER.info("###REQ: " + param);

            HttpEntity<String> requestStr = new HttpEntity<>(param.toString(), headers);

            ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.PUT, requestStr, String.class);

            LOGGER.info("###RES: " + result.getBody());
            
            return new JSONObject(result.getBody());
//        } catch (RestClientException ex) {
//            LOGGER.error("###ERROR: " + ex.getMessage());
        } catch (HttpClientErrorException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
            
            return new JSONObject(ex.getResponseBodyAsString());
        } catch (JSONException | RestClientException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
        
        return null;
    }
    
    public JSONObject postPengajuanPencairan(JSONObject param) {
//        String url = "https://apidev.tapera.go.id:8443/mitrapenyalur/v1/dev/pencairan/pengajuan";   //DEV
        String url = properties.getHost() + "/mitrapenyalur/v1/pencairan/pengajuan"; //PRD
        
        String token = postToken();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);
        
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_SERVER_HOST, PROXY_SERVER_PORT));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(proxy);
        
        ignoreCertificates();
        
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        try {
            LOGGER.info("###REQ: " + param);

            HttpEntity<String> requestStr = new HttpEntity<>(param.toString(), headers);

            String result = restTemplate.postForObject(url, requestStr, String.class);

            LOGGER.info("###RES: " + result);
            
            return new JSONObject(result);
        } catch (HttpClientErrorException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
            
            return new JSONObject(ex.getResponseBodyAsString());
        } catch (JSONException | RestClientException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
        
        return null;
    }
    
    public JSONObject postVerifikasiKedua(JSONObject param) {
//        String url = "https://apidev.tapera.go.id:8443/mitrapenyalur/v1/dev/verifikasi/final";  //DEV
        String url = properties.getHost() + "/mitrapenyalur/v1/verifikasi/final"; //PRD
        
        String token = postToken();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);
        
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_SERVER_HOST, PROXY_SERVER_PORT));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(proxy);
        
        ignoreCertificates();
        
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        try {
            LOGGER.info("###REQ: " + param);

            HttpEntity<String> requestStr = new HttpEntity<>(param.toString(), headers);

            String result = restTemplate.postForObject(url, requestStr, String.class);

            LOGGER.info("###RES: " + result);
            
            return new JSONObject(result);
        } catch (HttpClientErrorException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
            
            return new JSONObject(ex.getResponseBodyAsString());
        } catch (JSONException | RestClientException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
        
        return null;
    }
    
    public JSONObject postNotifSpk(JSONObject param) {
//        String url = "https://apidev.tapera.go.id:8443/mitrapenyalur/v1/dev/spk/notifikasi";    //DEV
        String url = properties.getHost() + "/mitrapenyalur/v1/spk/notifikasi"; //PRD
        
        String token = postToken();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);
        
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_SERVER_HOST, PROXY_SERVER_PORT));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(proxy);
        
        ignoreCertificates();
        
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        try {
            LOGGER.info("###REQ: " + param);

            HttpEntity<String> requestStr = new HttpEntity<>(param.toString(), headers);

            String result = restTemplate.postForObject(url, requestStr, String.class);

            LOGGER.info("###RES: " + result);
            
            return new JSONObject(result);
        } catch (HttpClientErrorException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
            
            return new JSONObject(ex.getResponseBodyAsString());
        } catch (JSONException | RestClientException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
        
        return null;
    }
    
    public JSONObject postVerifikasiPertama(JSONObject param) {
//        String url = "https://apidev.tapera.go.id:8443/mitrapenyalur/v1/dev/verifikasi/manfaat";    //DEV
        String url = properties.getHost() + "/mitrapenyalur/v1/verifikasi/manfaat"; //PRD
        
        String token = postToken();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);
        
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_SERVER_HOST, PROXY_SERVER_PORT));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(proxy);
        
        ignoreCertificates();
        
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        try {
            LOGGER.info("###REQ: " + param);

            HttpEntity<String> requestStr = new HttpEntity<>(param.toString(), headers);

            String result = restTemplate.postForObject(url, requestStr, String.class);

            LOGGER.info("###RES: " + result);
            
            return new JSONObject(result);
        } catch (HttpClientErrorException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
            
            return new JSONObject(ex.getResponseBodyAsString());
        } catch (JSONException | RestClientException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
        
        return null;
    }
    
    public JSONObject postNotifSlik(JSONObject param) {
//        String url = "https://apidev.tapera.go.id:8443/mitrapenyalur/v1/dev/slik/notifikasi";   //DEV
        String url = properties.getHost() + "/mitrapenyalur/v1/slik/notifikasi"; //PRD
        
        String token = postToken();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);
        
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_SERVER_HOST, PROXY_SERVER_PORT));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(proxy);
        
        ignoreCertificates();
        
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        try {
            LOGGER.info("###REQ: " + param);

            HttpEntity<String> requestStr = new HttpEntity<>(param.toString(), headers);

            String result = restTemplate.postForObject(url, requestStr, String.class);

            LOGGER.info("###RES: " + result);
            
            return new JSONObject(result);
        } catch (HttpClientErrorException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
            
            return new JSONObject(ex.getResponseBodyAsString());
        } catch (JSONException | RestClientException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
        
        return null;
    }
    
    public JSONObject postInquiryPeserta(JSONObject param) {
//        String url = "https://apidev.tapera.go.id:8443/mitrapenyalur/v1/dev/peserta/inquiry";   //DEV
        String url = properties.getHost() + "/mitrapenyalur/v1/peserta/inquiry"; //PRD
        
        String token = postToken();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);
        
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_SERVER_HOST, PROXY_SERVER_PORT));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(proxy);
        
        ignoreCertificates();
        
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        try {
            LOGGER.info("###REQ: " + param);

            HttpEntity<String> requestStr = new HttpEntity<>(param.toString(), headers);

            String result = restTemplate.postForObject(url, requestStr, String.class);

            LOGGER.info("###RES: " + result);
            
            return new JSONObject(result);
        } catch (HttpClientErrorException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
            
            return new JSONObject(ex.getResponseBodyAsString());
        } catch (JSONException | RestClientException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
        
        return null;
    }
    
    private String postToken() {
//        String url = "https://apidev.tapera.go.id:8443/security/oauth2/token";  //DEV
        String url = properties.getHost() + "/security/oauth2/token"; //PRD

        JSONObject tokenRequest = new JSONObject();
        tokenRequest.put("grant_type", "client_credentials");
//        tokenRequest.put("client_id", "t9kao8cyWBofVM4hYfvinyQe96jBoPL7");  //DEV
//        tokenRequest.put("client_secret", "Xz9ABobe8oAHSmNZ13WCjCeglRJ0MwAA");  //DEV
//        tokenRequest.put("client_id", "t9kao8cyWBofVM4hYfvinyQe96jBoPL7");  //PRD
//        tokenRequest.put("client_secret", "Xz9ABobe8oAHSmNZ13WCjCeglRJ0MwAA");  //PRD
        tokenRequest.put("client_id", properties.getClientId());
        tokenRequest.put("client_secret", properties.getClientSecret());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_SERVER_HOST, PROXY_SERVER_PORT));
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setProxy(proxy);
        
        ignoreCertificates();
        
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        try {
            LOGGER.info("###REQ_TOKEN: " + tokenRequest);

            HttpEntity<String> requestStr = new HttpEntity<>(tokenRequest.toString(), headers);

            String result = restTemplate.postForObject(url, requestStr, String.class);

            LOGGER.info("###RES_TOKEN: " + result);
            
            JSONObject tokenResult = new JSONObject(result);
            
            if (tokenResult.has("access_token")) {
                return tokenResult.getString("access_token");
            }
        } catch (HttpClientErrorException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        } catch (JSONException | RestClientException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
        
        return null;
    }
    
    private void ignoreCertificates() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (KeyManagementException | NoSuchAlgorithmException ex) {
            LOGGER.error("###ERROR: " + ex.getMessage());
        }
    }
}
