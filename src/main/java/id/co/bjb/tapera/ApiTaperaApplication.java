/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bjb.tapera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;

/**
 *
 * @author LENOVO
 */
@SpringBootApplication
@EnableCaching
public class ApiTaperaApplication extends SpringBootServletInitializer {
    
    public static void main(String[] args) {
        SpringApplication.run(ApiTaperaApplication.class, args);
    }
}
